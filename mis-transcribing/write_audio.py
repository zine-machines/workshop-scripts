#!/usr/bin/env python3


import speech_recognition as sr
from os import path


# obtain audio from the microphone
r = sr.Recognizer()
with sr.Microphone() as source:
    audio = r.listen(source)

# write audio to a WAV file
with open("mic_audio.wav", "wb") as f:
    f.write(audio.get_wav_data())


# current_directory = path.dirname(path.realpath(__file__))
# audio_file_path = path.join(current_directory, "mic_audio.wav")

# recognizer = sr.Recognizer()

# with sr.AudioFile(audio_file_path) as source:
#     audio = recognizer.record(source)
#     try:
#         print(recognizer.recognize_sphinx(audio))
#     except sr.UnknownValueError:
#         print("Sphinx could not understand audio")
#     except sr.RequestError as e:
#         print("Sphinx error; {0}".format(e))