# mis-transcribing

This script uses speech recognition software to transcribe your voice reading texts or saying things.

## Run It:
* Transcribing audio from the microphone.
Talk into the microphone.Then run the script.
```bash
$ python3 transcribing_from_mic.py>transcription_mic.txt
```

* Transcribing audio from youtube video.
Download the audio:
```bash
$ youtube-dl -x --audio-format wav https://www.youtube.com/watch?v=oVX2ehoUuws
```
Rename the audio file to: audio.wav

Run the script:
```bash
$ python3 transcribing_from_youtube.py>transcription_youtube.txt
```
