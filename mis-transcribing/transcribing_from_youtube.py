#!/usr/bin/env python3

# NOTE: this example requires PyAudio because it uses the Microphone class

import speech_recognition as sr
from os import path


current_directory = path.dirname(path.realpath(__file__))
audio_file_path = path.join(current_directory, "audio.wav")

recognizer = sr.Recognizer()

with sr.AudioFile(audio_file_path) as source:
    audio = recognizer.record(source)
    try:
        print(recognizer.recognize_sphinx(audio))
    except sr.UnknownValueError:
        print("Sphinx could not understand audio")
    except sr.RequestError as e:
        print("Sphinx error; {0}".format(e))

