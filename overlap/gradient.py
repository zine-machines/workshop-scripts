from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.pagesizes import landscape
from reportlab.lib.colors import (
    darkorchid, peachpuff, tan, teal,
    orangered, papayawhip, royalblue,
    red, green, blue, yellow, cornsilk,
    rosybrown
)
from reportlab.lib.units import mm

canvas = Canvas("gradient.pdf")
canvas.setPageSize((300*mm, 300*mm))
canvas.linearGradient(10*mm, 20*mm, 190*mm, 100*mm, (teal, papayawhip))
canvas.showPage()
canvas.save()
