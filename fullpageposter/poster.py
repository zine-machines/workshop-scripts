"""A poster generating script.

We look through all titles on Wikipedia and ask which ones have the word `zine`
or `machine` in them. We then pass them through a pipeline to get a nice A3
poster with some styling. In the end, we have the following set of steps:

Get Titles -> Parse Titles -> Clean Titles -> Write HTML -> Style HTML -> Print PDF

Happy Hacking.

"""  # noqa

from urllib.parse import unquote

from jinja2 import Environment, FileSystemLoader
from weasyprint import CSS, HTML


TITLES_TXT = 'datasets/titles.txt'
POSTER_HTML = 'generated/poster.html'
POSTER_PDF = 'generated/poster.pdf'
STYLES_CSS = 'styles/style.css'
JINJA_TEMPLATE = 'base.html.j2'
TEMPLATES_FOLDER = 'templates'


def get_titles():
    """Get the titles from 'titles.txt'."""
    return open(TITLES_TXT).read().split()


def parse_titles(titles):
    """Get titles with 'zine' and 'machine' in it."""
    answer = input('Filter text for words `zine_` and `machine_`? [y/n] ')
    if answer in ('y', 'Y'):
        return [
            title for title in titles
            if '_zine' in title
            or '_machine' in title
        ]
    else:
        return titles


def clean_titles(titles):
    """Remove all the cruft from the titles."""
    cleaned_titles = []

    for title in titles:
        unquoted = unquote(title)
        replaced = unquoted.replace('_', ' ')
        cleaned_titles.append(replaced)

    return cleaned_titles


def generate_template(titles):
    """Generate a HTML page with Jinja2."""
    template_loader = FileSystemLoader(searchpath=TEMPLATES_FOLDER)
    environment = Environment(loader=template_loader)
    template = environment.get_template(JINJA_TEMPLATE)

    with open(POSTER_HTML, 'w') as handle:
        rendered = template.render({'titles': " ".join(titles)})
        handle.write(rendered)


def generate_pdf():
    """Generate a PDF poster from the HTML poster."""
    html = HTML(filename=POSTER_HTML)
    css = CSS(string=open(STYLES_CSS).read())
    html.write_pdf(POSTER_PDF, stylesheets=[css])


if __name__ == "__main__":
    """The entry point. Run the functions we want."""
    titles = get_titles()

    parsed = parse_titles(titles)
    cleaned = clean_titles(parsed)

    generate_template(cleaned)
    generate_pdf()
