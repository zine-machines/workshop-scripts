#!/usr/bin/env python3
import subprocess, re, shutil

"""
examples

count pages

    pdftk_utils.py test.pdf count

pad pdf to a multiple of a given number of pages (uses make_blank_pdf.py/reportlab)

    pdftk_utils.py test.pdf pad --multiple 4 --output padded.pdf

"""

def count_pages (path):
    p = subprocess.Popen(["pdftk", path, "dump_data"], stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
    out, _ = p.communicate()
    out = out.decode("utf-8")
    m = re.search("^NumberOfPages: (\d+)", out, flags=re.M)
    return int(m.group(1))


def pad_to_multiple (path, multiple, output, alwaysOutput=True):
    import make_blank_pdf, tempfile, os
    n = count_pages(path)
    cur_pages = n
    while n % multiple != 0:
        n += 1
    pages_to_add = n - cur_pages
    if pages_to_add > 0:
        _, tmp = tempfile.mkstemp(".pdf")
        make_blank_pdf.make_blank_pdf(tmp, pages_to_add)
        # print ("pdftk A=\"{0}\" B=\"{1}\" cat A B output {2}".format(path, tmp, args.output))
        p = subprocess.Popen(["pdftk", "A={0}".format(path), "B={0}".format(tmp), "cat", "A", "B", "output", args.output], stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
        out, _ = p.communicate()
        out = out.decode("utf-8")
        os.remove(tmp)
        return output
    elif alwaysOutput:
        # in the event of no need for padding, simply copy the input file
        shutil.copyfile(path, output)
        return output

if __name__ == "__main__":
    import argparse
    ap = argparse.ArgumentParser("")
    ap.add_argument("path")
    subparsers = ap.add_subparsers()
    count = subparsers.add_parser("count")
    count.set_defaults(func=count_pages)

    pad = subparsers.add_parser("pad")
    pad.add_argument("--multiple", type=int, default=None)
    pad.add_argument("--output", default="padded.pdf")
    pad.set_defaults(func=pad_to_multiple)

    args = ap.parse_args()
    oargs = dict(vars(args))
    del oargs['func']
    print (args.func(**oargs))