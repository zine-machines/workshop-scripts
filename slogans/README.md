# Slogan Generator

Generate slogans from presidential speeches:

> http://capitolwords.org/about/

Or, provide a `input.txt` file and generate from your own text:

> https://www.gutenberg.org/

## Run It

```bash
$ python3 generator.py  # run the script
$ weasyprint slogan.html -s templates/style.css slogan.pdf  # generate the PDF
```

You can turn this PDF into a booklet using the booklet folder script.
