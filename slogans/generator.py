"""A slogan generator from presidential candidate speeches."""

import os
import random

import spacy
from textacy.datasets import CapitolWords
from jinja2 import Environment, FileSystemLoader


SLOGAN_HTML = 'slogan.html'
STYLES_CSS = 'templates/style.css'
TEMPLATES_FOLDER = 'templates'
JINJA_TEMPLATE = 'base.html.j2'


def word_from_tag(tag, doc):
    """Get random words that match the tag."""
    try:
        return random.choice([
            item.text for item in doc
            if str.isalpha(item.text.lower())
            and item.pos_ == tag
        ])
    except Exception:
        print('Oops, something went wrong. Feed me more input!')


def make_slogans(doc):
    """Run the slogan generator on a tokenized document."""
    forms = [
        # See https://spacy.io/api/annotation#section-pos-tagging
        ['ADJ', 'DET', 'NOUN'],
        ['NOUN', 'ADP', 'NOUN'],
        ['NOUN', 'DET', 'NOUN'],
        ['ADV', 'ADV', 'NOUN'],
        ['ADJ', 'NOUN', 'ADP', 'ADJ', 'NOUN'],
        ['VERB', 'ADV', 'VERB', 'VERB'],
        ['DET', 'NOUN', 'NUM', 'VERB', 'DET', 'NOUN', 'ADP', 'DET'],
        ['PRON', 'VERB', 'NOUN', 'ADV'],
        ['NOUN', 'VERB', 'ADV', 'VERB']
    ]
    slogan_list = []
    for form in forms:
        for _ in range(0, 10):
            slogan = ' '.join([word_from_tag(tag, doc) for tag in form])
            slogan_list.append(slogan)
    return slogan_list


def generate_template(slogans):
    """Generate a HTML page with Jinja2."""
    template_loader = FileSystemLoader(searchpath=TEMPLATES_FOLDER)
    environment = Environment(loader=template_loader)
    template = environment.get_template(JINJA_TEMPLATE)
    with open(SLOGAN_HTML, 'w') as handle:
        rendered = template.render({'slogans': slogans})
        handle.write(rendered)


if __name__ == "__main__":
    nlp = spacy.load('en_core_web_sm')

    if os.path.exists('input.txt'):
        print('Reading your input.txt ...')
        text_input = ' '.join(open('input.txt', 'r').readlines())
    else:
        print('Reading from Capitol Word speeches ...')
        candidate = random.choice(list(CapitolWords.speaker_names))
        speeches = CapitolWords().records(speaker_name=candidate, limit=50)
        text_input = ' '.join(speech['text'] for speech in speeches)

    document = nlp(text_input)
    slogans = make_slogans(document)
    generate_template(slogans)
