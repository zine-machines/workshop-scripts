# patterns

Generate patterns from text!

The input is the `output.txt` file with whatever text you want.

The output is the `pattern.txt`, with the pattern.

## Run It

```bash
$ python3 carlandre.py  # just see if out on the screen
$ python3 carlandre.py > patterns.txt  # put it in a file
```
