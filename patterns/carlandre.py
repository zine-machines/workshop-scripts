from math import ceil


def pop_items(words, num_items):
    ''' Removes num_items from words.'''
    if not words:
        return [], []

    if num_items > len(words):
        raise ValueError('Not enough items!')

    popped = []
    for number in range(num_items):
        removed = words.pop(0)
        popped.append(removed)
    return popped, words


def all_words_less_than(words, maxlength):
    ''' Checks if the words have the correct length given in maxlength'''
    for word in words:
        if len(word) > maxlength:
            return False
    return True


def filterwords(words, maxlength):
    ''' Puts the words which have the correct length in a new list '''
    goodwords = []
    for word in words:
        if len(word) <= maxlength and len(word) >= 2:
            goodwords.append(word)
    return goodwords


def pattern(words, maxlength):
    goodwords = filterwords(words, maxlength)
    items_pattern = maxlength + (maxlength - 4)

    if len(goodwords) % items_pattern != 0:
        rest = len(goodwords) % items_pattern
        difference = len(goodwords) - rest
        goodwords = goodwords[:difference]

    times = int(len(words) / items_pattern)

    final_pattern = []
    for each_time in range(times):
        popped, whatisleft = pop_items(goodwords, items_pattern)
        if not popped:
            continue
        goodwords = whatisleft

        middle = ceil(len(popped)/2)

        ascending = sorted(popped[:middle], key=len)
        descending = sorted(popped[middle:], key=len, reverse=True)

        sorted_pattern = ascending + descending
        final_pattern.append(sorted_pattern)

    return final_pattern


if __name__ == '__main__':
    with open('input.txt', 'r') as handle:
        contents = handle.read()
    splitted = contents.split()

    ll = (pattern(splitted, 8))
    for l in ll:
        for x in l:
            print(x)
        print()
