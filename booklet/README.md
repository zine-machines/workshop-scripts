# PDF booklet creator

A recipe that turns any PDF into a ready-to-print booklet.

## Run It

```bash
$ cp my-cool-thing.pdf booklet  # copy your PDF into the booklet folder
$ cd booklet  # go into the booklet folder
$ make my-cool-thing.booklet.pdf  # run the script
$ evince my-cool-thing.booklet.pdf  # open up your new booklet
```
