#!/usr/bin/env python3

import argparse, sys
from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.pagesizes import letter, A4
from reportlab.lib.units import inch, cm
# from reportlab.pdfbase.ttfonts import TTFont, pdfmetrics


def make_blank_pdf (path, pages, size=A4):
    c = Canvas(path, pagesize=size)
    for i in range(pages):
        c.setPageSize(size)
        c.showPage()
    c.save()
    return True

if __name__ == "__main__":
    ap = argparse.ArgumentParser("")
    ap.add_argument("output")
    ap.add_argument("--number", "-n", type=int, default=1)
    ap.add_argument("--size", default=None)
    args = ap.parse_args()
    size = A4
    if args.size:
        if args.size.lower() == "letter":
            size = letter
    make_blank_pdf(args.output, args.number, size)
