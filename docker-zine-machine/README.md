# docker-zine-machine

A little computer with everything we need pre-installed.

## Run It

```bash
$ make run
```

## Build It

```bash
$ make build
```

## Push It

```bash
$ docker login
$ docker push
```
