# workshop-scripts

## Enter The Zine Machine

Install Docker and Git with:

  * https://docs.docker.com/install/
    * click on the left sidebar: Linux/MacOS/Microsoft Windows
    * If you need to log in to download, use the following credentials:
      * username: zinemachinessign / password: zinemachines_sign

  * https://git-scm.com/downloads

Then enter the Zine Machine with:

```bash
$ git clone https://gitlab.constantvzw.org/zine-machines/workshop-scripts.git && cd workshop-scripts
$ docker run -itv ${PWD}:/workshop zinemachines/workshop
```

## The Scripts

### /booklet/
Turns any multiple page pdf into a ready-to-print booklet pdf.

### /chopperzine/
Chop 3 images into a head,body,legs experiment.

### /coordinates/
Export coordinates from routes in maps and print only the lines in a PDF.

### /flipbooks/
Makes a ready-to-print flipbook out of any YouTube video.

### /fullpageposter/
Make full page posters from text (just like our workshop poster).

### /mastodon_bot/
A way to publish some text or images to Mastodon using [our Mastodon bot].

### /mis-transcribing/
Using speech recognition software to transcribe your voice reading texts or
saying things.

### /patterns/
Generate patterns from text inspired from Carl Andre.

### /scripts_to_pdf/
Generates a zine from all the workshop scripts.

### /slogans/
Generates random slogans from famous speeches of American presidents.

### /sound_to_image/
Converts a sound to raw data, and then to image.

### /text_to_right/
A script to convert a text in one sentence text and positions it to the right
of a landscape page in a column.

[our Mastodon bot]: https://post.lurk.org/@zine_machines
