from pykml import parser
import re
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas


def translate(value, leftMin, leftMax, rightMin, rightMax):
    # Figure out how 'wide' each range is
    leftSpan = leftMax - leftMin
    rightSpan = rightMax - rightMin

    # Convert the left range into a 0-1 range (float)
    valueScaled = float(value - leftMin) / float(leftSpan)

    # Convert the 0-1 range into a value in the right range.
    return rightMin + (valueScaled * rightSpan)


kml_file = 'coordinates.kml'
all_routes = {}
with open(kml_file) as f:
    doc = parser.parse(f).getroot().Document.Folder

for pm in doc.iterchildren():
    if hasattr(pm, 'LineString'):
        name = pm.name.text
        coordinates = pm.LineString.coordinates.text
        cordinatesinline = coordinates.split('\n')
        cordinatesasarray = []
        for line in cordinatesinline:
                pattern = re.compile("^\s+|\s*,\s*|\s+$")
                array = [x for x in pattern.split(line) if x]
                if array:
                        cordinatesasarray.append(array)
        all_routes[name] = cordinatesasarray


canvas = canvas.Canvas("generated/route_kml.pdf", pagesize=letter)
canvas.setLineWidth(.8)
canvas.setFont('Helvetica', 6)
for name in all_routes:
        x, y = [], []
        for points in all_routes[name]:
                x.append(points[0])
                y.append(points[1])

        i = 0
        for a in x:
                if i < len(x)-1:
                    x_new = translate(float(x[i]), float(min(x)),
                                      float(max(x)), 50, 500)

                    y_new = translate(float(y[i]), float(min(y)),
                                      float(max(y)), 50, 500)

                    x_new2 = translate(float(x[i+1]), float(min(x)),
                                       float(max(x)), 50, 500)

                    y_new2 = translate(float(y[i+1]), float(min(y)),
                                       float(max(y)), 50, 500)

                    print(x_new, y_new, x_new2, y_new2)
                    canvas.line(x_new, y_new, x_new2, y_new2)

                    i = i + 1

        canvas.drawString(10, 45, name)
        canvas.showPage()

canvas.save()
