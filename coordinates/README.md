# Coordinates:

Export coordinates from routes in maps and convert to PDF layouts.

## Run It

Create a route in (or use routes you have already created):

  * https://www.google.com/maps
  * https://www.openstreetmap.org/


Export the `.kml` or `.gpx` files with your route.

Then save them in this folder and name them:

  * coordinates.kml  # google maps
  * coordinates.gpx  # open street maps

Run one of the two. It will generate a PDF with the route:

```bash
# google maps route
$ python2 KMLcoordinates-to-lines.py  # run the script
$ evince generated/route_kml.pdf  # open the PDf

# open street maps route
$ python3 GPXcoordinates-to-lines.py  # run the script
$ evince generated/route_gpx.pdf  # open the PDF
```
