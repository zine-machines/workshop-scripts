import gpxpy
import gpxpy.gpx
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas


gpx_file = open('coordinates.gpx', 'r')
gpx = gpxpy.parse(gpx_file)


def translate(value, leftMin, leftMax, rightMin, rightMax):
    # Figure out how 'wide' each range is
    leftSpan = leftMax - leftMin
    rightSpan = rightMax - rightMin

    # Convert the left range into a 0-1 range (float)
    valueScaled = float(value - leftMin) / float(leftSpan)

    # Convert the 0-1 range into a value in the right range.
    return rightMin + (valueScaled * rightSpan)


canvas = canvas.Canvas("generated/route_gpx.pdf", pagesize=letter)
canvas.setLineWidth(.8)
canvas.setFont('Helvetica', 6)

for track in gpx.tracks:
    for segment in track.segments:
        x, y = [], []
        for point in segment.points:
            x.append(point.latitude)
            y.append(point.longitude)

    i = 0
    for a in x:
        if i < len(x)-1:
            x_new = translate(float(x[i]), float(min(x)),
                              float(max(x)), 50, 500)

            y_new = translate(float(y[i]), float(min(y)),
                              float(max(y)), 50, 500)

            x_new2 = translate(float(x[i+1]), float(min(x)),
                               float(max(x)), 50, 500)

            y_new2 = translate(float(y[i+1]), float(min(y)),
                               float(max(y)), 50, 500)

            print(x_new, y_new, x_new2, y_new2)

            canvas.line(x_new, y_new, x_new2, y_new2)

            i = i+1

    canvas.drawString(10, 45, track.name)
    canvas.showPage()

canvas.save()
