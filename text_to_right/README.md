# text_to_right

A script to convert a text in one sentence text and positions it to the right
of a landscape page in a column on a PDF page.

## Run It

```bash
# create your text.txt file ...
$ python text_to_right.py  # run the script
$ evince generated/page.pdf  # open the PDF
```
