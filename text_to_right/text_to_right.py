"""
This script converts a text into a single sentence.
It then positions it to the right of a landscape HTML page.
We can use this tecnique to move text into different positions.
"""

from urllib.parse import unquote

from jinja2 import Environment, FileSystemLoader
from weasyprint import CSS, HTML
import subprocess

subprocess.call('mkdir -p generated', shell=True)

text_TXT = 'text.txt'
PAGE_HTML = 'generated/page.html'
PAGE_PDF = 'generated/page.pdf'
STYLES_CSS = 'style.css'
JINJA_TEMPLATE = 'base.html.j2'
TEMPLATES_FOLDER = 'templates'


def get_text():
    """Get the text from 'text.txt'."""
    return open(text_TXT).read().split()


def generate_template(text):
    """Generate a HTML page with Jinja2."""
    template_loader = FileSystemLoader(searchpath=TEMPLATES_FOLDER)
    environment = Environment(loader=template_loader)
    template = environment.get_template(JINJA_TEMPLATE)

    with open(PAGE_HTML, 'w') as handle:
        rendered = template.render({'text': " ".join(text)})
        handle.write(rendered)


def generate_pdf():
    """Generate a PDF PAGE from the HTML PAGE."""
    html = HTML(filename=PAGE_HTML)
    css = CSS(string=open(STYLES_CSS).read())
    html.write_pdf(PAGE_PDF, stylesheets=[css])


if __name__ == "__main__":
    """The entry point. Run the functions we want."""
    text = get_text()
    generate_template(text)
    generate_pdf()
