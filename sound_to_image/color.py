import math
from PIL import Image
from colour import Color


width = 256
height = 256
image = Image.new('RGB', (width, height))
loaded_image = image.load()

for xnum in range(width):
    r, g, b = 40, 120, 10
    for ynum in range(height):
        loaded_image[xnum, ynum] = r, g, b
        r, g, b = r + 1, g + 1, b + 1

image.save('test2.png', 'PNG')

for ynum in range(width):
    a, b, c = 100, 120, 140
    for xnum in range(height):
        loaded_image[ynum, xnum] = a, b, c
        a, b, c = a + 1, b + 1, c + 1

image.save('test3.png', 'PNG')
