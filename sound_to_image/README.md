Convert any sound/tone/voice into raw data, and then into image. Afterwards, superimpose a generated color gradient on that image, for some nice visuals.


##Install


pip install reportlab

##Use

Use a sound recording program or sound generator such as audacity to get an audio file in the .wav format

Run sound_image.py to convert your .wav file into .dat, then into jpg/png

$ python sound_image.py


##Gradients 

Run color_rep.py to create your gradient. Play around with the numbers/colors to get the desired effect.

$ python color_rep.py

#or

Run color.py to create a gradient that fades to white

$ python color.py


##Combining the two

First, if your gradient in in a pdf format, convert it to png/jpg

$ convert -density 300 -depth 8 -quality 85 gradient.pdf <file>.png


##Overlaying the two images for some nice visual effect

$ composite -blend 80 <gradient>.jpg <file>.jpg <newfile>.jpg




