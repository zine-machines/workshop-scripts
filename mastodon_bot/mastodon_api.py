from mastodon import Mastodon
import sys

filename=sys.argv[1]
filetype=sys.argv[2]

api = Mastodon('CLIENT_ID', 'CLIENT_SECRET','ACCESS_TOKEN',api_base_url="your-instance-url")

# it opens the textfile you have included in the same folder and posts the text
if filetype=='txt':
	with open(filename) as f:
		api.toot(f)
	f.close()
# it opens the image you have included in the same folder and posts the image
else:
	v=api.media_post(filename)
	api.status_post('',media_ids=v)
