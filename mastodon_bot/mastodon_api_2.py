from os import environ
from mastodon import Mastodon
import sys

filename = sys.argv[1]
filetype = sys.argv[2]

api = Mastodon(
    '8d60edd634524023380c42e8d24649f0c4f9c3fda511021d0e4e1309e71cbc3e',
    'c3122b7828617a88fd9f5c7c8add16ac421924d62d108614a4f50f81df5d4639',
    'f730751613b5dcbbacfcba0adbc55d488a4e3355e88376ca47e5398834b3dd4d',
    api_base_url="https://post.lurk.org"
)

if filetype=='txt':
    with open(filename) as file_handle:
        api.toot(file_handle.read())
else:
    media = api.media_post(filename)
    api.status_post('', media_ids=media)
