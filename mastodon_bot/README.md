# Mastodon Bot

This script uploads the results of our scripts to Mastodon through a bot account.

> https://post.lurk.org/@zine_machines

## Run It

Include the text or images you want to post inside the same folder with the script.

```bash
# posting a text toot
$ cp cool-file.txt mastodon_bot # put your text file in the folder
$ python mastodon_api.py # post the text to mastodon

# posting an image
$ cp cool-file.png mastodon_bot # put your image file in the folder
$ python mastodon_api.py # post the image to mastodon
```
