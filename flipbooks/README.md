# flipbooks

Programmatically generate flipbooks from Youtube videos.

## Run It

```bash
$ python3 flipbook.py "https://www.youtube.com/watch?v=yti8UPPC0JE"  # run the script
$ evince generated/flipbook.pdf  # open the new flipbook
```

## Deeper Down the Rabbit Hole

  * https://github.com/rg3/youtube-dl/blob/master/README.md#readme
  * https://github.com/kkroening/ffmpeg-python
  * https://www.imagemagick.org/Usage/crop/#border
  * https://www.papersizes.org/a-sizes-in-pixels.htm
  * https://github.com/ImageMagick/ImageMagick/issues/396
