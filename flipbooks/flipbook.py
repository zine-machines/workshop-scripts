"""Generate flipbooks from Youtube videos.

Usage:
    flipbook.py URL
"""

from docopt import docopt

import math
import os
import shutil
import subprocess


NUMBER_OF_PAGES = 0
BASE_DIR = os.path.dirname(os.path.realpath(__file__))


def get_youtube_video(url, generated_dir):
    """Download a youtube video."""
    downloaded_path = os.path.join(generated_dir, 'downloaded.mp4')

    if os.path.exists(downloaded_path) and confirm(downloaded_path) == 'n':
        return downloaded_path

    subprocess.call((
        'youtube-dl',
        url,
        '-o',
        downloaded_path,
        '-f',
        'bestvideo',
        '--no-continue'  # overwrite
    ))

    return downloaded_path


def chop_video(downloaded_path, generated_dir):
    """Cut a video between the specified times"""
    chopped_path = os.path.join(generated_dir, 'chopped.mp4')

    if os.path.exists(chopped_path):
        answer = confirm(chopped_path)
        if answer == 'n':
            return chopped_path
        os.remove(chopped_path)

    example = '(format is MM:SS, like 03:00): '
    start = input('Please enter start time {}'.format(example))
    end = input('Please enter end time {}'.format(example))

    subprocess.call((
        '/usr/bin/ffmpeg',
        '-i',
        downloaded_path,
        '-ss',
        '00:{}'.format(start),
        '-to',
        '00:{}'.format(end),
        chopped_path,
        '-y',
    ))

    return chopped_path


def video_to_frames(chopped_path, generated_dir):
    """Convert the video to frames."""
    frames_path = os.path.join(generated_dir, 'frames')

    if os.path.exists(frames_path):
        if confirm(frames_path) == 'y':
            shutil.rmtree(frames_path)
        else:
            return frames_path

    question = 'Please enter desired frame rate (5 is a good default): '
    number_frames = input(question)

    os.mkdir(frames_path)

    subprocess.call((
        '/usr/bin/ffmpeg',
        '-i',
        chopped_path,
        '-qscale:v',
        '2',
        '-r',
        number_frames,
        '{}/frames%2d.jpg'.format(frames_path),
        '-y',
    ))

    return frames_path


def prune_frames(frames_path):
    """Delete extra frames."""
    global NUMBER_OF_PAGES

    frames = os.listdir(frames_path)

    # Since we're going with 8 frames per page we need to keep the frame count
    # as a multiple of 8 (so all frames fit evenly onto all pages). So, here,
    # we round down the number frames to a multiple of 8. This is pretty hairy.
    NUMBER_OF_PAGES = int(8 * math.floor(len(frames) / 8.))
    frames_to_prune = sorted(
        frames, key=lambda frame:
        int(''.join(filter(str.isdigit, frame)))
    )[NUMBER_OF_PAGES:]

    for frame_file in frames_to_prune:
        path = os.path.join(frames_path, frame_file)
        os.remove(path)


def convert_frames_to_pdf(frames_path):
    """Convert frames to video with ImageMagick."""
    prune_frames(frames_path)

    converted_path = os.path.join(generated_dir, 'converted.pdf')

    if os.path.exists(converted_path):
        answer = confirm(converted_path)
        if answer == 'n':
            return converted_path
        os.remove(converted_path)

    subprocess.call(
        '/usr/bin/convert $(ls -v *.jpg) -rotate 90 -quality 100 {}'.format(
            converted_path
        ), shell=True, cwd=frames_path
    )

    return converted_path


def reorder_pdf(converted_path, generated_dir):
    """Re-order the PDF pages for printing order."""
    global NUMBER_OF_PAGES

    # In order to discover the right way to order the pages correctly we need
    # to know what multiple of 8, and hence, what number of PDF pages we're
    # dealing with. Then we can arrange the order so that they are easy to cut
    # and fold into a flip book afterwards.
    step = int(NUMBER_OF_PAGES / 8)
    initial_pattern = [n+1 for n in range(0, NUMBER_OF_PAGES, step)]

    generated_pattern = []
    for number_to_add in range(0, step):
        for pattern_number in initial_pattern:
            next_number = pattern_number + number_to_add
            generated_pattern.append(next_number)
    joined_order_list = ', '.join(map(str, generated_pattern))

    reordered_path = os.path.join(generated_dir, 'reordered.pdf')

    if os.path.exists(reordered_path):
        answer = confirm(reordered_path)
        if answer == 'n':
            return reordered_path
        os.remove(reordered_path)

    print((
        'Sometimes, the scale of the image that the `pdfjam` program '
        'transfers onto the PDF during re-ordering is not enough to fit '
        'the PDF page. So, please experiment with setting a scale that '
        'works for your video. We have seen good results with a value of 1.5!'
    ))
    chosen_scale = input('Choose your image scale (try 1.5): ')

    # Note, we're using `shell=True` because `subprocess.call` seems to, for
    # some reason, re-order the order of the arguments I pass to `pdfjam`. That
    # is strange and breaks things.
    subprocess.call(
        "/usr/bin/pdfjam {} '{}' --scale {} --outfile {}".format(
            converted_path,
            joined_order_list,
            chosen_scale,
            reordered_path
        ),
        shell=True
    )

    return reordered_path


def run_imposition(reordered_path, generated_dir):
    generated_path = os.path.join(BASE_DIR, generated_dir, 'flipbook.pdf')
    output_path = os.path.join(BASE_DIR, 'reordered-nup.pdf')

    if os.path.exists(generated_path):
        answer = confirm(generated_path)
        if answer == 'n':
            return generated_path
        os.remove(generated_path)

    subprocess.call((
        'pdfnup',
        '--nup',
        '4x2',
        reordered_path
    ))

    # Since `pdfnup` is not so configurable, it just dumps the file wherever it
    # wants. We want everything in the `generated` directory. So, we move it
    # here.
    shutil.move(output_path, generated_path)

    return generated_path


def confirm(path, prompt='Re-create it'):
    """Prompt user if they want to run the command."""
    question = '{} already exists. {}? [y/n]: '.format(path, prompt)
    return input(question)


if __name__ == "__main__":
    """The auto-magical main function. Run it!"""
    arguments = docopt(__doc__, version='flipbook.py 0.0.1')

    generated_dir = os.path.join(BASE_DIR, 'generated')

    print('Downloading the Youtube video now ...')
    downloaded_path = get_youtube_video(arguments['URL'], generated_dir)

    print('Cutting the video to match start/end times ...')
    chopped_path = chop_video(downloaded_path, generated_dir)

    print('Converting video to frames ...')
    frames_path = video_to_frames(chopped_path, generated_dir)

    print('Converting image frames onto PDF pages ...')
    converted_path = convert_frames_to_pdf(frames_path)

    print('Re-ordering PDF pages to match imposition ...')
    reordered_path = reorder_pdf(converted_path, generated_dir)

    print('Running PDF imposition for final PDF file ...')
    generated_path = run_imposition(reordered_path, generated_dir)

    print('-' * 79)
    print('Generated file available at {}'.format(generated_path))
    print('-' * 79)
