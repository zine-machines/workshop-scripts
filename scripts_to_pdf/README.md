# Scripts to PDF

Generate slogans from presidential speeches.

## Run It

```bash
$ python generator.py  # run the script
$ evince generated/scripts.pdf  # open the PDF
```
