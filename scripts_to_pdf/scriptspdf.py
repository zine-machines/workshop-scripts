"""Creates a zine from all the workshop scripts!"""

import os, sys
from jinja2 import Environment, FileSystemLoader
from weasyprint import CSS, HTML

SCRIPTS_HTML = 'generated/scripts.html'
STYLES_CSS = 'templates/style.css'
TEMPLATES_FOLDER = 'templates'
JINJA_TEMPLATE = 'base.html.j2'
SCRIPTS_PDF = 'generated/scripts.pdf'

path = SCRIPTS_PATH
dirs = os.listdir(path)

def generate_template(scripts):
    """Generate a HTML page with Jinja2."""
    template_loader = FileSystemLoader(searchpath=TEMPLATES_FOLDER)
    environment = Environment(loader=template_loader)
    template = environment.get_template(JINJA_TEMPLATE)
    with open(SCRIPTS_HTML, 'w') as handle:
        rendered = template.render({'scripts': scripts, 'titles':titles})
        handle.write(rendered)

def generate_pdf():
    """Generate a PDF poster from the HTML poster."""
    html = HTML(filename=SCRIPTS_HTML)
    css = CSS(string=open(STYLES_CSS).read())
    html.write_pdf(SCRIPTS_PDF, stylesheets=[css])

if __name__ == "__main__":
    titles, scripts = [], []

    for root, dirs, files in os.walk(path):
        for name in files:
            if name.endswith(".txt"):
                contents=open(os.path.join(root, name)).read()
                scripts.append(contents)
                titles.append(name)

    generate_template(scripts)
    generate_pdf()
