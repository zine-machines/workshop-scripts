"""Chop things up"""

import subprocess

if __name__ == "__main__":
    for name in ['denis.jpg', 'luke.jpg', 'marieke.jpg']:
        magic = {
            'head_{}'.format(name): '450x150+0+0',
            'body_{}'.format(name): '450x300+0+150',
            'legs_{}'.format(name): '450x350+0+450',
        }

        subprocess.call(
            'convert -rotate -90 {} {}rotate.jpg'.format(name, name),
            shell=True
        )
        image = '{}rotate.jpg'.format(name)

        for part, number in magic.items():
            command = 'convert {file} -crop {num} {out}.jpg'.format(
                file=image,
                num=number,
                out=part
            )
            print("Running this command: {}".format(command))
            subprocess.call(command, shell=True)

    subprocess.call('convert -append head_denis.jpg.jpg body_luke.jpg.jpg legs_marieke.jpg.jpg mess.jpg', shell=True)
